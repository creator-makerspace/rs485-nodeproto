/*
 * RS485NodeProto.cpp
 *
 *  Created on: Mar 21, 2016
 *      Author: johnny
 */

#include <Arduino.h>
#include <util/crc16.h>

#include "RS485NodeProto.h"

// Byte values used for special stuff
#define STARTBYTE   0xA0
#define ESCBYTE     0xE0
#define STOPBYTE    0x80
#define ESCMASK		0xFF

// Defines number of milliseconds before returning to idle, after no bytes received.
#define TIMEOUT_IDLE   		300

// Defines minimum timeout from stop byte received until TX can be done at the earliest.
#define TIMEOUT_STOP_TX		10

// Defines the MAX delay in milliseconds before retrying a send operation.
#define RETRY_DELAY_MAX		100


void RS485NodeProto::begin() {
	_rsctrl.begin();
}

void RS485NodeProto::end() {
	_rsctrl.begin();
}

RS485NodeProto::SendResult RS485NodeProto::sendPacket(bool multicast, uint8_t address, char data[], uint8_t length, uint8_t retry) {
	// Check current RX state. If data is currently being received, we can not send data.
	if (_isRXBusy()) return RS485NodeProto::SEND_RX_BUSY;

	// Add 1 to retry, as we want to send at least once.
	retry++;

	// The current result. Initialized, as a warning is generated if not, even if init will always happen!
	RS485NodeProto::SendResult currResult = RS485NodeProto::SEND_OK;

	// Sends packet to specified node address
	while (retry > 0) {
		// Check for collision before we do anything
		if (_rsctrl.available()) return RS485NodeProto::SEND_RX_BUSY;

		// Execute a send and return if retries are exhausted
		currResult = _sendPacketRaw(multicast ? PTYPE_MULTICAST : PTYPE_SINGLE, address, data, length);
		--retry;

		// Check for a collision
		if (currResult == RS485NodeProto::SEND_COLLISION) {
			// Wait 10 ms, plus a random delay (10 ms steps)
			if (retry) _delayRandom();
			continue;
		}

		// Multicast messages are not ACKed.
		if (multicast) break;

		// Wait for ACK. If NACK, retry
		RS485NodeProto::RecvStatus ackRcvStatus;
		uint16_t timeout = RETRY_DELAY_MAX;

		// Try to receive an ACK packet
		ackRcvStatus = _receivePacket(timeout);

		// Anything except data ready, means we need to retry. Continue...
		if (ackRcvStatus != RS485NodeProto::RECV_DATA_READY) continue;

		// If we received a valid packet. If it's an ACK, we are done and break.
		if (inPacket.type == RS485PktType::PTYPE_ACK) break;

		// If not ACK nor NACK, or we got a non-OK packet status, we got a collition on our hands, as someone else sent data out of band.
		if (inPacket.type < RS485PktType::PTYPE_ACK || inPacket.pktStatus != PKTSTATUS_OK) {
			return RS485NodeProto::SEND_COLLISION;
		}

		// The received packet was a valid NACK, we simply wrap around and retry. which means we should retry...
	}

	return currResult;
}

RS485NodeProto::RecvStatus RS485NodeProto::receivePacket(uint16_t timeout) {
	RS485NodeProto::RecvStatus rcvStatus;

	while (true) {
		// Call internal receive packet handler.
		rcvStatus = _receivePacket(timeout);

		// Return if IDLE, BUSY or TIMEOUT. In sync mode, only TIMEOUT can happen.
		if (rcvStatus < RECV_DATA_READY) return rcvStatus;

		// This means we have received a packet for us. If this is an ACK or NACK, we should ignore it because "send" is responsible for this.
		// NOTE: This is even done on ERROR packets, as we cannot NACK a received ACK or NACK anyway.
		if (inPacket.type >= RS485PktType::PTYPE_ACK) continue;

		// Check for single addressed packet, which should be acked or nacked.
		if (inPacket.type == RS485PktType::PTYPE_SINGLE) {
			// Now, we either have good data or an error, which should be ACKed or NACKed respectively.
			// This may result in a collition, but ACKs are fire and forget.
			_sendPacketRaw(rcvStatus == RECV_ERROR ? RS485PktType::PTYPE_NACK : RS485PktType::PTYPE_ACK, inPacket.srcNodeId, NULL, 0);
		}

		// Break if data is ready or in async mode. Continue looping on errors.
		if (rcvStatus == RECV_DATA_READY || timeout == 0) break;
	}

	return rcvStatus;
}

// ================== Protected Methods =======================

inline void RS485NodeProto::_delayRandom() {
	delay(random(1, RETRY_DELAY_MAX / TIMEOUT_STOP_TX) * TIMEOUT_STOP_TX);
}

uint16_t RS485NodeProto::_sendEscByteCRC(uint16_t currcrc, uint8_t db, bool noCrc) {
	// Update CRC
	if (!noCrc) currcrc = _crc_ccitt_update(currcrc, db);

	// Check for escaping
	switch (db) {
		case STARTBYTE:
		case ESCBYTE:
		case STOPBYTE:
			// Send escape byte and modify data
			_rsctrl.stream.write(ESCBYTE);
			db ^= ESCMASK;
			break;
	}
	// Write the data byte
	_rsctrl.stream.write(db);

	return currcrc;
}

RS485NodeProto::SendResult RS485NodeProto::_sendPacketRaw(RS485PktType pktType, uint8_t address, char data[], uint8_t length) {
	uint8_t txByte;

	// Enable TX mode, with loop back
	bool useLoopback = _rsctrl.setTransmit(!_noColDet);

	// Send START byte
	_rsctrl.stream.write(STARTBYTE);

	// Check if loopback is enabled
	if (useLoopback) {
		_rsctrl.stream.flush();

		// Check if we receive the same byte as we sent
		if (_rsctrl.stream.available()) {
			txByte = _rsctrl.stream.read();
			if (txByte != STARTBYTE) {
				// We have a collision. End transmission and return COLLITION error.
				_rsctrl.endTransmit();
				return RS485NodeProto::SEND_COLLISION;
			}
		}
		// Collision check is done, continue transmit without loop back
		_rsctrl.setTransmit(false);
	}

	// Prepare the Destination header byte
	txByte = pktType | (address & 0x3F);
	uint16_t txcrc = _sendEscByteCRC(0, txByte);

	// Prepare the Source Header byte
	txByte = (_nodeId & 0x3F);
	txcrc = _sendEscByteCRC(txcrc, txByte);

	// Send the packet data...
	for (uint8_t i = 0; i < length; i++) {
		txcrc = _sendEscByteCRC(txcrc, data[i]);
	}

	// Send the CRC
	_sendEscByteCRC(0, txcrc >> 8,   true);
	_sendEscByteCRC(0, txcrc & 0xFF, true);

	// Send the STOP byte
	_rsctrl.stream.write(STOPBYTE);

	// End transmit
	_rsctrl.endTransmit();

	// Return OK status
	return RS485NodeProto::SEND_OK;
}

RS485NodeProto::RecvStatus RS485NodeProto::_receivePacket(uint16_t &timeout) {
	RS485NodeProto::RecvStatus rcvStatus; // = RS485NodeProto::RECV_IDLE;
	unsigned long startrcvtime = millis();
	unsigned long checktime = 0;

	while (true) {
		// Simply execute the receive until we have a packet or an error.
		rcvStatus = _recvUpdate();

		// In async operations, we simply do not loop
		if (timeout == 0) return rcvStatus;

		// Capture the time, to check for timeout
		checktime = millis() - startrcvtime;

		// If status is higher than busy, it means we either have a packet or an error.
		if (rcvStatus > RS485NodeProto::RECV_BUSY) break;

		// Check for timeout
		if (checktime > timeout) {
			rcvStatus = RS485NodeProto::RECV_TIMEOUT;
			timeout = 0;
			break;
		}
	}

	// Decrease the timeout in case we are called again.
	if (timeout > 0) {
		if (checktime < timeout) timeout -= checktime;
		else timeout = 1;
	}

	return rcvStatus;
}

RS485NodeProto::RecvStatus RS485NodeProto::_recvUpdate() {
	uint8_t rcvByte;
	RS485NodeProto::RecvStatus recvStatus = RECV_IDLE;

	// Loop as long as there is data available
	while (_rsctrl.stream.available()) {
		// Read the current byte
		rcvByte = _rsctrl.stream.read();
		_rxLastByte = millis();

		// Stop bytes must be handled specially, as they can happen any time (except in Escapes)
		if (recvStatus == RECV_IDLE) recvStatus = RECV_BUSY;
		if (!_rxEscState && rcvByte == STOPBYTE) {
			// If we where in data receive mode
			if (_rxState != RX_RCVDATA) {
				// Go directly to STOP except if we are in RCVDATA Mode.
				_rxState = RX_STOP;

				// In case of an error here (overflow), we should return that status
				if (recvStatus == RECV_ERROR) return recvStatus;

				// Continue, as there are not much more to do.
				continue;
			}
			else {
				// Change state to VERIFY
				_rxState = RX_VERIFY;
			}
		}
		else {
			// Check for escapes
			if (rcvByte == ESCBYTE && _rxState > RX_IDLE) {
				_rxEscState = true;
				continue;
			}
			if (_rxEscState) {
				rcvByte ^= ESCMASK;
				_rxEscState = false;
			}
		}

		// Used for CRC checks
		uint16_t rxCrcVerify;

		// Determine action based on state
		switch(_rxState) {
		case RX_IDLE:
		case RX_STOP:
			// No state change if we get a stop byte.
			if (rcvByte == STOPBYTE) break;
			if (rcvByte != STARTBYTE) {
				// Go directly to IGNORE mode if not a start byte.
				_rxState = RX_IGN;
				break;
			}

			// We got a start byte, so we should continue to read DESTINATION address
			_rxState = RX_HDEST;
			break;

		case RX_HDEST:
			// Check for Single or Multicast packet
			if ((rcvByte & 0xC0) == PTYPE_MULTICAST) {
				// Check Multicast mask
				if (((rcvByte & 0x3F) & _nodeId) != (rcvByte & 0x3F)) {
					// Nope, mask does not match
					_rxState = RX_IGN;
					break;
				}
			}
			// Not Multicast, so check if the address is for us
			else if ((rcvByte & 0x3F) != _nodeId) {
				// Nope it's not. Ignore following data.
				_rxState = RX_IGN;
				break;
			}

			// Yup, the packet is for us. Record the packet type and clear the buffer
			inPacket.length = 0;
			inPacket.type = (RS485PktType)(rcvByte & 0xC0);
			_rxState = RX_HSRC;

			// Do the initial CRC calculation
			_rxCrc16 = _crc_ccitt_update(0, rcvByte);
			break;

		case RX_HSRC:
			// The packet is for us, so simply get the source address, reset length and continue.
			inPacket.srcNodeId = rcvByte & 0x3F;
			inPacket.length = 0;
			_rxState = RX_RCVDATA;
			_rxCrc16 = _crc_ccitt_update(_rxCrc16, rcvByte);
			break;

		case RX_RCVDATA:
			// Check for buffer overflow.
			if (inPacket.length >= RS485_RX_BUFFERSIZE) {
				// Indicate that there was a transmission error, and set state to ignore.
				inPacket.pktStatus = PKTSTATUS_ERR_OVERFLOW;
				recvStatus = RECV_ERROR;
				_rxState = RX_IGN;
				break;
			}

			inPacket.buffer[inPacket.length++] = rcvByte;
			if (inPacket.length > 2) {
				_rxCrc16 = _crc_ccitt_update(_rxCrc16, inPacket.buffer[inPacket.length - 3]);
			}
			break;

		case RX_VERIFY:
			// Packet reception is complete. Verify CRC.
			rxCrcVerify = ((uint16_t)inPacket.buffer[inPacket.length - 2] << 8) | inPacket.buffer[inPacket.length - 1];
			inPacket.length -= 2;

			// Verify CRC
			if (rxCrcVerify == _rxCrc16) {
				// Set status OK
				inPacket.pktStatus = PKTSTATUS_OK;
				_sendPacketRaw(PTYPE_ACK, inPacket.srcNodeId, NULL, 0);
				recvStatus = RECV_DATA_READY;
			}
			else {
				// Set status CRC error
				inPacket.pktStatus = PKTSTATUS_ERR_CRC;
				_sendPacketRaw(PTYPE_NACK, inPacket.srcNodeId, NULL, 0);
				recvStatus = RECV_ERROR;
			}
			// Change state to STOP, and return status immediately (which is OK or ERROR)
			_rxState = RX_STOP;
			return recvStatus;

		case RX_IGN:
		default:
			// Catches IGNORE, which simply drops bytes
			break;
		}
	}

	// Check if RX state should be reset to IDLE, due to a timeout
	if (_rxState != RX_IDLE && millis() - _rxLastByte > TIMEOUT_IDLE) {
		_rxState = RX_IDLE;
	}

	return recvStatus;
}
