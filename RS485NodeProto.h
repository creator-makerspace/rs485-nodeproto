/*
 * RS485NodeProto.h
 *
 * Protocol designed for RS-485, which supports Master/Slave and Multi-master
 * topologies.
 *
 *  Created on: Mar 21, 2016
 *      Author: johnny
 */

#ifndef LIB_RS485LIB_RS485NODEPROTO_H_
#define LIB_RS485LIB_RS485NODEPROTO_H_

#include "RS485Controller.h"

#define RS485_RX_BUFFERSIZE 	128


/**
 * Defines the Packet Type to send
 */
enum RS485PktType      { PTYPE_SINGLE = 0x00, PTYPE_MULTICAST = 0x40, PTYPE_ACK = 0x80, PTYPE_NACK = 0xC0 };

/**
 * Defines status of received packets
 */
enum RS485PktStatus    { PKTSTATUS_OK = 0x00, PKTSTATUS_ERR_CRC, PKTSTATUS_ERR_OVERFLOW };

/**
 * Structure used for receiving packets
 */
struct RS485Packet {
	RS485PktType 	type;
	RS485PktStatus	pktStatus;	/* Indicates error in packet reception. 0 = no error */
	uint8_t 	 	srcNodeId;
	uint8_t		 	length;
	uint8_t  	 	buffer[RS485_RX_BUFFERSIZE];
};

class RS485NodeProto {
public:
	/**
	 * Defines the Send Result returned by sendPacket:
	 *
	 * - SEND_OK - Data was sent, and acknowledged if required
	 * - SEND_COLLISION - Data was not sent due to a collision (on last retry)
	 * - SEND_NOACK     - Data was sent, but not acknowledged
	 * - SEND_RX_BUSY   - Data cannot be sent, because an RX is in progress
	 * - SEND_TIMEOUT   - Bus has been busy for too long.
	 */
	enum SendResult { SEND_OK, SEND_COLLISION, SEND_NOACK, SEND_RX_BUSY, SEND_TIMEOUT };

	/**
	 * Defines the Receive status returned by recvPacket:
	 *
	 * - RECV_IDLE 		 - No data in progress
	 * - RECV_BUSY 		 - Data reception is in progress, but has not completed (in async mode only)
	 * - RECV_DATA_READY - Data is received and ready
	 * - RECV_ERROR      - Data was received, but had errors.
	 */
	enum RecvStatus { RECV_IDLE = 0, RECV_BUSY = 1, RECV_TIMEOUT = 2, RECV_DATA_READY = 3, RECV_ERROR = 4  };

	/**
	 * Initializes the RS485 Node Protocol
	 *
	 * @param ctrl The RS485 Controller to use for protocol handling
	 * @param nodeId The current node ID, from 0 to 63 (will be concatenated)
	 * @param retry (optional) The number of retries to execute if a MESSAGE packet is not Acknowledged. Default 3 times
	 * @param noCollitionDetect (optional) If true, no collition detection will be attempted
	 */
	RS485NodeProto(RS485Controller& ctrl, const uint8_t nodeId, const uint8_t retry = 3, const bool noCollitionDetect = false) :
		_rsctrl(ctrl), _nodeId(nodeId & 0x3F), _noColDet(noCollitionDetect)
	{
		_rxState = RX_IDLE;
		_rxEscState = false;
		_rxLastByte = 0;
		_rxCrc16 = 0;
	}

	/**
	 * Calls the wrapped RS485Controller instance begin() or end() functions
	 */
	void begin(), end();

	/**
	 * Sends a packet to the Node which has the ID specified in address.
	 *
	 * Behavior depends on the pktType parameter:
	 * - RELIABLE packets requires an ACK to be received before sendPacket returns (or retry attempts are exhausted)
	 * - UNRELIABLE will only be sent once, and sendPacket will not wait for an ACK. False is returned only on detected collisions.
	 * - MULTICAST will be retried if a collision is detected, but does not wait for an ACK
	 * - MULTICAST_UNRELIABLE will be send once, and returns false only if a collision is detected
	 *
	 * When a Multicast packet is sent, the address defines a bit-mask which defines which nodes should receive
	 * the packet. A node will receive the packet if (nodeId & bit-mask == bit-mask), and discard the packet otherwise.
	 * To send a packet to all nodes, use address 0x3F (the 3 MSBs are ignored so 0xFF work fine as well).
	 *
	 * @param multicast The packet will be multicasted. No ACK will be checked.
	 * @param address The Node ID to send the packet to, or bit-mask for multicast packets
	 * @param data The data to send
	 * @param length The length of the data to send
	 * @return The result of the send operation
	 */
	SendResult sendPacket(bool multicast, uint8_t address, char data[], uint8_t length, uint8_t retry = 0);

	/**
	 * Receives a packet from the sender.
	 *
	 * The timeout parameter defines how long to wait for a packet before returning. If timeout is 0,
	 * the method will return as soon as possible. In this case, the method must be called continously
	 * in order to receive data. This mode is useful if other tasks needs handling in between.
	 *
	 * @param timeout: Defines millisecond timeout before the method returns even if a packet is not received.
	 * @return The status of the receive operation, RECV_DATA_READY indicates that data is received.
	 */
	RecvStatus receivePacket(uint16_t timeout);

	/**
	 * Exposes the packet received. The data will only be valid after update() has returned
	 * "true", and only up till update is called again. Using this data except in this condition
	 * will give unpredictable results
	 */
	struct RS485Packet inPacket;

protected:
	/**
	 * Internal RX handling state. Note that bit 1 is used for detecting RX BUSY.
	 */
	enum RS485RxState { RX_IDLE = 0x00,
						RX_IGN = 0x01,
						RX_STOP = 0x02,
						RX_HDEST = 0x03,
						RX_HSRC = 0x05,
						RX_RCVDATA = 0x07,
						RX_VERIFY = 0x09
	};

	RS485Controller& 	_rsctrl;
	const uint8_t 		_nodeId;
	const bool 			_noColDet;

	RS485RxState 		_rxState;
	bool 				_rxEscState;
	unsigned long 		_rxLastByte;
	uint16_t 			_rxCrc16;

	/**
	 * Convenience function used to retermine if RX is busy.
	 */
	inline bool _isRXBusy() {
		return _rxState & 0x01;
	}

	/**
	 * Convenience function used to delay for a random time period before retrying a send operation.
	 */
	inline void _delayRandom();

	/**
	 * Sends a single byte, and escapes it if necessary
	 */
	uint16_t _sendEscByteCRC(uint16_t currcrc, uint8_t db, bool noCRC = false);


	/**
	 * Internal function which handles the actual data packet transmission. This function will only
	 * do the sending, and ignores the current internal state machine.
	 */
	SendResult _sendPacketRaw(RS485PktType pktType, uint8_t address, char data[], uint8_t length);

	/**
	 * Internal function for receiving a packet. Used both internally and externally.
	 */
	RecvStatus _receivePacket(uint16_t &timeout);

	/**
	 * Internal function which handles data reception
	 */
	RecvStatus _recvUpdate();


};



#endif /* LIB_RS485LIB_RS485NODEPROTO_H_ */
