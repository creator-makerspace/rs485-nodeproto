/*
 * rs485generic.h
 *
 * Generic API for RS485 module hanlding. Can be used to wrap any Stream instance,
 * which means it can be used for both Software and Hardware serials.
 *
 *  Created on: Mar 21, 2016
 *      Author: johnny
 */

#ifndef LIB_RS485LIB_RS485CONTROLLER_H_
#define LIB_RS485LIB_RS485CONTROLLER_H_

#include <Stream.h>

class RS485Controller {
public:

	/**
	 * Creates a new RS485 controller instance, defining the Read-Enable and Output Enable pins
	 *
	 * If RE and WE pins are the same, only one will be used.
	 *
	 * @param stream The Stream instance to use
	 * @param repin Pin number for Read-Enable (Active Low)
	 * @param wepin Pin number for Write-Enable (Active High)
	 */
	RS485Controller(Stream& stream, uint8_t repin, uint8_t wepin)
		: stream(stream), _repin(repin), _wepin(wepin) {}

	/**
	 * Used to initialize the controller, setting the RE and WE pins as output and
	 * setting up Read-Mode (both pins low).
	 */
	void begin();

	/**
	 * Reconfingures the control pins as input.
	 */
	void end();

	/**
	 * Starts a transmission or changes loopback mode.
	 *
	 * Sets the WE and RE pin high. If loopback is true, the RE pin is kept low, so data is
	 * looped back. If both pins are the same, loopback has no effect.
	 *
	 * @param loopback If true, the RE pin will be kept high resulting in outputted data to be looped back.
	 * @return True if loopback will be active, and false if not
	 */
	bool setTransmit(bool loopback);

	/**
	 * Ends transmission, and sets WE and RE pins high.
	 * This will flush the serial output buffer, to make sure the WE mode is
	 * disabled at the correct time.
	 */
	void endTransmit();

	/**
	 * Convenience method for checking if data is avaialble in stream
	 */
	inline bool available() {
		return stream.available();
	}

	/**
	 * Publically exposed for convenience
	 */
	Stream &stream;

protected:
	uint8_t _repin, _wepin;

	inline void _setCtrlPinMode(uint8_t mode);


};



#endif /* LIB_RS485LIB_RS485CONTROLLER_H_ */
