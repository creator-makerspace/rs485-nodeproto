/*
 * RS485Controller.cpp
 *
 * Implements the RS485 controller class
 *
 *  Created on: Mar 21, 2016
 *      Author: johnny
 */

#include <Arduino.h>
#include "RS485Controller.h"

inline void RS485Controller::_setCtrlPinMode(uint8_t mode) {
	pinMode(_wepin, mode);
	if (_wepin != _repin) {
		pinMode(_repin, mode);
	}
}

// ==== Public API ====

void RS485Controller::begin() {
	_setCtrlPinMode(OUTPUT);
	// This will in effect reset the control pins
	endTransmit();
}

void RS485Controller::end() {
	_setCtrlPinMode(INPUT);
}

bool RS485Controller::setTransmit(bool loopback) {
	digitalWrite(_wepin, HIGH);
	if (!loopback && _wepin != _repin) {
		digitalWrite(_repin, HIGH);
		return false;
	}
	return _wepin != _repin;
}

void RS485Controller::endTransmit() {
	stream.flush();
	digitalWrite(_wepin, LOW);
	if (_wepin != _repin) {
		digitalWrite(_repin, LOW);
	}
}
