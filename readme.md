# RS485 Nodeprotocol

## Using in a PlatformIO project

Either add

```
library_install = 73
```

To your platformio.ini (Note, this will pull the lib's master branch)

## PlatformIO - Using via a submodule or folder in the lib directory

Or if you are a more daring soul to use the develop branch then you can simply

```
git submodule add https://gitlab.com/creator-makerspace/rs485-nodeproto lib/rs485-nodeproto
```

Or you can just keep a copy of the rs485-nodeproto tree in your lib folder.

And include the headers as usually.
